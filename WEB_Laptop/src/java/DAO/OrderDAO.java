/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import DTO.AccountDTO;
import DTO.Cart;
import DTO.ItemDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;

/**
 *
 * @author msi
 */
public class OrderDAO extends DBcontext {

    public void addOrder(AccountDTO a, Cart cart, String address) {
        LocalDate curDate = LocalDate.now();
        String date = curDate.toString();
        try {
            //add order
            String sql = "INSERT INTO ORDERS VALUES (?,?,?,?,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, a.getAccountID());
            st.setDouble(2, cart.getTotalMoney());
            st.setString(3, date);
            st.setString(4, address);
            st.setInt(5, 0);
            st.executeUpdate();
            //lay id cua order vua add
            String sql_1 = "select top 1 OrderID from Orders order by OrderID desc";
            PreparedStatement st1 = connection.prepareStatement(sql_1);
            ResultSet rs = st1.executeQuery();
            if (rs.next()) {
                int oid = rs.getInt(1);
                for (ItemDTO i : cart.getItems()) {
                    String sql_2 = "INSERT INTO OrderDetail VALUES (?,?,?,?,?)";
                    PreparedStatement st2 = connection.prepareStatement(sql_2);
                    st2.setInt(1, i.getProduct().getProductID());
                    st2.setInt(2, oid);
                    st2.setInt(3, i.getQuantity());
                    st2.setDouble(4, i.getProduct().getPrice() /*tru di tien discount*/);
                    st2.executeUpdate();
                }
            }
            //cap nhat lai so luong san pham
            String sql_3 = "UPDATE Product SET QuantityAvailable = QuantityAvailable - ? WHERE ProductID = ?";
            PreparedStatement st3 = connection.prepareStatement(sql_3);
            for (ItemDTO i : cart.getItems()) {
                st3.setInt(1, i.getQuantity());
                st3.setInt(2, i.getProduct().getProductID());
                st3.executeUpdate();
            }
        } catch (Exception e) {
        }
    }
}
