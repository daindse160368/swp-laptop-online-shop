/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import DTO.ProductDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author msi
 */
public class ProductDAO extends DBcontext {

    public List<ProductDTO> getAllProduct() {
        String sql = "SELECT * FROM Product";
        List<ProductDTO> lp = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ProductDTO product = new ProductDTO();
                product.setProductID(rs.getInt(1));
                product.setCategoryID(rs.getInt(2));
                product.setName(rs.getString(3));
                product.setQuantity(rs.getInt(4));
                product.setPrice(rs.getInt(5));
                product.setRam(rs.getString(6));
                product.setStorage(rs.getString(7));
                product.setCpu(rs.getString(8));
                product.setVga(rs.getString(9));
//                product.setStatus(rs.getInt(10));
                lp.add(product);
            }
        } catch (Exception e) {
        }
        return lp;
    }

    public ProductDTO getProductByID(int id) {
        String sql = "SELECT * FROM Product WHERE ProductID = " + id;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                ProductDTO product = new ProductDTO();
                product.setProductID(rs.getInt(1));
                product.setCategoryID(rs.getInt(2));
                product.setName(rs.getString(3));
                product.setQuantity(rs.getInt(4));
                product.setPrice(rs.getInt(5));
                product.setRam(rs.getString(6));
                product.setStorage(rs.getString(7));
                product.setCpu(rs.getString(8));
                product.setVga(rs.getString(9));
                return product;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void addProduct(int CateID, String name, int quantity, double price, String ram, String storage, String cpu, String vga, int Status) {
        String sql = "";
    }

    public List<ProductDTO> getProductByCategory(int id) {
        String sql = "SELECT p.ProductID, p.Name, p.QuantityAvailable, p.UnitPrice, p.UnitPrice, p.RamCapacity, p.StorageCapacity, p.VgaBrand FROM Product p\n"
                + "JOIN Category c\n"
                + "ON c.CategoryID = p.CategoryID \n"
                + "WHERE c.CategoryID = ?";
        List<ProductDTO> lp = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ProductDTO pro = new ProductDTO();
                pro.setProductID(rs.getInt(1));
                pro.setName(rs.getString(2));
                pro.setQuantity(rs.getInt(3));
                pro.setPrice(rs.getInt(4));
                pro.setRam(rs.getString(5));
                pro.setStorage(rs.getString(6));
                pro.setCpu(rs.getString(7));
                pro.setVga(rs.getString(8));
                lp.add(pro);
            }
        } catch (Exception e) {
        }
        return lp;
    }

    public List<ProductDTO> searchByKey(String key) {
        List<ProductDTO> list = new ArrayList<>();
        String sql = "SELECT * FROM Product WHERE Name LIKE '%" + key + "%'";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ProductDTO product = new ProductDTO();
                product.setProductID(rs.getInt(1));
                product.setCategoryID(rs.getInt(2));
                product.setName(rs.getString(3));
                product.setQuantity(rs.getInt(4));
                product.setPrice(rs.getInt(5));
                product.setRam(rs.getString(6));
                product.setStorage(rs.getString(7));
                product.setCpu(rs.getString(8));
                product.setVga(rs.getString(9));
//                product.setStatus(rs.getInt(10));
                list.add(product);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public static void main(String[] args) {
        ProductDAO pd = new ProductDAO();
        System.out.println(pd.getProductByCategory(4).size());
    }
}
