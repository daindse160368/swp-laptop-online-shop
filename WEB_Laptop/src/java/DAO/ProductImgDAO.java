/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import DTO.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author win
 */
public class ProductImgDAO extends DBcontext {

    public List<ProductImgDTO> getImg() {
        String sql = "SELECT * FROM ProductIMG";
        List<ProductImgDTO> lp = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ProductImgDTO pimg = new ProductImgDTO();
                pimg.setImgid(rs.getInt(1));
                pimg.setPid(rs.getInt(2));
                pimg.setPath(rs.getString(3));

                lp.add(pimg);
            }
        } catch (Exception e) {
        }
        return lp;
    }

    public ProductImgDTO getPImgByPid(int pid) {
        String sql = "SELECT TOP 1 * FROM ProductIMG WHERE ProID=?";
        ProductImgDTO pimg = new ProductImgDTO();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, pid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                pimg.setImgid(rs.getInt(1));
                pimg.setPid(rs.getInt(2));
                pimg.setPath(rs.getString(3));
            }
        } catch (Exception e) {
        }
        return pimg;
    }
}
