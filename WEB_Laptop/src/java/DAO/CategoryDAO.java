/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import DTO.CategoryDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author msi
 */
public class CategoryDAO extends DBcontext {

    public List<CategoryDTO> getAllCategory() {
        String sql = "SELECT * FROM Category";
        ArrayList<CategoryDTO> lc = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                CategoryDTO category = new CategoryDTO();
                category.setCategoryID(rs.getInt(1));
                category.setName(rs.getString(2));
                category.setDateCreated(rs.getString(3));
                category.setDateModified(rs.getString(4));
                category.setStatus(rs.getInt(5));
                lc.add(category);
            }
        } catch (Exception e) {
        }
        return lc;
    }
    public List<CategoryDTO> displayCategoryinHome() {
        String sql = "SELECT * FROM Category WHERE Status = 1";
        ArrayList<CategoryDTO> lc = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                CategoryDTO category = new CategoryDTO();
                category.setCategoryID(rs.getInt(1));
                category.setName(rs.getString(2));
                category.setDateCreated(rs.getString(3));
                category.setDateModified(rs.getString(4));
                category.setStatus(rs.getInt(5));
                lc.add(category);
            }
        } catch (Exception e) {
        }
        return lc;
    }

    public void addCategory(String name, String date, int Status) {
        String sql = "INSERT INTO [Category] (Name, DateCreated, Status) VALUES (?,?,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, date);
            ps.setInt(3, Status);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public CategoryDTO getCategoryByColumnName(String columnName, String input) {
        String sql = "SELECT * FROM Category WHERE [" + columnName + "] = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, input);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                CategoryDTO category = new CategoryDTO();
                category.setName(rs.getString(2));
                category.setStatus(rs.getInt(5));
                return category;
            }
        } catch (Exception e) {

        }
        return null;
    }

    public void changeStatusCategoryByID(int status, int id) {
        String sql = "UPDATE Category SET Status = ? WHERE CategoryID  = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, status);
            ps.setInt(2, id);
             ps.executeUpdate();
        } catch (Exception e) {
        }
    }

}
