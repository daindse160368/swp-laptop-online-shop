/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import DTO.AccountDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author msi
 */
public class AccountDAO extends DBcontext {

    public AccountDTO checkLogin(String username, String password) {
        String sql = "SELECT * FROM Account WHERE Username = ? AND Password = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                AccountDTO account = new AccountDTO();
                account.setAccountID(rs.getInt(1));
                account.setUsername(rs.getString(2));
                account.setPassword(rs.getString(3));
                account.setFullname(rs.getString(4));
                account.setPhone(rs.getString(5));
                account.setEmail(rs.getString(6));
                account.setAddress(rs.getString(7));
                account.setRole(rs.getInt(8));
                return account;
            }
        } catch (Exception e) {

        }
        return null;
    }

    public void register(String username, String password, String Fullname, String Email, int role) {
        String sql = "INSERT INTO [dbo].[Account]\n"
                + "           ([Username]\n"
                + "           ,[Password]\n"
                + "           ,[Fullname]\n"
                + "           ,[Email]\n"
                + "           ,[Role])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setString(3, Fullname);
            ps.setString(4, Email);
            ps.setInt(5, role);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public AccountDTO searchAccountByColumn(String columnName, String input) {
        String sql = "SELECT * FROM Account WHERE [" + columnName + "] = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, input);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                AccountDTO account = new AccountDTO();
                account.setAccountID(rs.getInt(1));
                account.setUsername(rs.getString(2));
                account.setPassword(rs.getString(3));
                account.setFullname(rs.getString(4));
                account.setPhone(rs.getString(5));
                account.setEmail(rs.getString(6));
                account.setAddress(rs.getString(7));
                account.setRole(rs.getInt(8));
                return account;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public List<AccountDTO> getAllAccount() {
        String sql = "SELECT * FROM Account";
        ArrayList<AccountDTO> la = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                AccountDTO account = new AccountDTO();
                account.setAccountID(rs.getInt(1));
                account.setUsername(rs.getString(2));
                account.setPassword(rs.getString(3));
                account.setFullname(rs.getString(4));
                account.setPhone(rs.getString(5));
                account.setEmail(rs.getString(6));
                account.setAddress(rs.getString(7));
                account.setRole(rs.getInt(8));
                la.add(account);
            }
        } catch (Exception e) {
        }
        return la;
    }

    public static void main(String[] args) {
        AccountDAO dao = new AccountDAO();
        System.out.println(dao.getAllAccount().size());
    }
}
