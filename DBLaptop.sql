USE [SWP_Laptop]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 5/27/2023 5:39:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[AccountID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](255) NULL,
	[Password] [varchar](255) NULL,
	[Fullname] [nvarchar](255) NULL,
	[Phone] [varchar](255) NULL,
	[Email] [varchar](255) NULL,
	[Address] [varchar](255) NULL,
	[Role] [int] NULL,
 CONSTRAINT [PK__Account__349DA5863230729C] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 5/27/2023 5:39:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NULL,
	[DateCreated] [nvarchar](50) NULL,
	[DateModified] [nvarchar](50) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK__Category__19093A2BC80CD5FD] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 5/27/2023 5:39:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedback](
	[FeedbackID] [int] IDENTITY(1,1) NOT NULL,
	[OrderID] [int] NULL,
	[Title] [varchar](255) NULL,
	[Context] [text] NULL,
	[DateCreate] [datetime] NULL,
	[StarVoted] [int] NULL,
 CONSTRAINT [PK__Feedback__6A4BEDF6AA3F4FF7] PRIMARY KEY CLUSTERED 
(
	[FeedbackID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 5/27/2023 5:39:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[OrderDetailID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NULL,
	[OrderID] [int] NULL,
	[Quantity] [int] NULL,
	[UnitPrice] [decimal](10, 2) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[OrderDetailStatus] [varchar](255) NULL,
 CONSTRAINT [PK__OrderDet__D3B9D30C2B927113] PRIMARY KEY CLUSTERED 
(
	[OrderDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 5/27/2023 5:39:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NULL,
	[TotalPrice] [decimal](10, 2) NULL,
	[OrderDate] [datetime] NULL,
	[Address] [varchar](255) NULL,
	[Status] [varchar](255) NULL,
 CONSTRAINT [PK__Orders__C3905BAFA3821AA2] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Post]    Script Date: 5/27/2023 5:39:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Post](
	[PostID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NULL,
	[Title] [varchar](255) NULL,
	[Description] [varchar](255) NULL,
	[Context] [text] NULL,
	[DateCreated] [nvarchar](50) NULL,
	[DateModified] [nvarchar](50) NULL,
 CONSTRAINT [PK__Post__AA1260383E58F344] PRIMARY KEY CLUSTERED 
(
	[PostID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PostImg]    Script Date: 5/27/2023 5:39:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostImg](
	[ImgID] [int] NOT NULL,
	[PostID] [int] NULL,
	[Path] [nvarchar](50) NULL,
 CONSTRAINT [PK_PostImg] PRIMARY KEY CLUSTERED 
(
	[ImgID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 5/27/2023 5:39:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryID] [int] NULL,
	[Name] [varchar](255) NULL,
	[QuantityAvailable] [int] NULL,
	[UnitPrice] [decimal](10, 2) NULL,
	[RamCapacity] [varchar](255) NULL,
	[StorageCapacity] [varchar](255) NULL,
	[CpuBrand] [varchar](255) NULL,
	[VgaBrand] [varchar](255) NULL,
 CONSTRAINT [PK__Product__B40CC6ED02628E8B] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductIMG]    Script Date: 5/27/2023 5:39:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductIMG](
	[ImgID] [int] IDENTITY(1,1) NOT NULL,
	[ProID] [int] NULL,
	[Path] [nvarchar](50) NULL,
 CONSTRAINT [PK_ProductIMG] PRIMARY KEY CLUSTERED 
(
	[ImgID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Voucher]    Script Date: 5/27/2023 5:39:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Voucher](
	[VoucherID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NULL,
	[VoucherCode] [varchar](255) NULL,
	[DateStart] [datetime] NULL,
	[DateEnd] [datetime] NULL,
	[TypeID] [int] NULL,
 CONSTRAINT [PK__Voucher__3AEE79C1DD6D132A] PRIMARY KEY CLUSTERED 
(
	[VoucherID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VoucherProductLink]    Script Date: 5/27/2023 5:39:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VoucherProductLink](
	[VoucherID] [int] NULL,
	[ProductID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VoucherType]    Script Date: 5/27/2023 5:39:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VoucherType](
	[TypeID] [int] IDENTITY(1,1) NOT NULL,
	[NameType] [varchar](255) NULL,
	[DiscountNumber] [int] NULL,
 CONSTRAINT [PK__VoucherT__516F0395E73E4872] PRIMARY KEY CLUSTERED 
(
	[TypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VoucherUsage]    Script Date: 5/27/2023 5:39:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VoucherUsage](
	[VoucherID] [int] NULL,
	[OrderID] [int] NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Account] ON 

INSERT [dbo].[Account] ([AccountID], [Username], [Password], [Fullname], [Phone], [Email], [Address], [Role]) VALUES (1, N'admin', N'123', N'admin', NULL, N'admin123@gmail.com', NULL, 1)
INSERT [dbo].[Account] ([AccountID], [Username], [Password], [Fullname], [Phone], [Email], [Address], [Role]) VALUES (9, N'trungkien', N'123', N'Trung Kien', NULL, N'kiennt7902@gmail.com', NULL, 2)
SET IDENTITY_INSERT [dbo].[Account] OFF
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (3, N'ASUS', N'', NULL, 1)
INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (4, N'DELL', N'', NULL, 1)
INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (5, N'MSI', N'26/05/2023', NULL, 1)
INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (11, N'APPLE', N'26/05/2023', NULL, 1)
INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (12, N'SAMSUNG', N'26/05/2023', NULL, 1)
INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (13, N'LG', N'26/05/2023', NULL, 1)
INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (14, N'XIAOMI', N'27/05/2023', NULL, 1)
INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (15, N'A', N'27/05/2023', NULL, 0)
INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (16, N'B', N'27/05/2023', NULL, 0)
INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (17, N'C', N'27/05/2023', NULL, 0)
INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (18, N'D', N'27/05/2023', NULL, 0)
INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (19, N'E', N'27/05/2023', NULL, 0)
INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (20, N'F', N'27/05/2023', NULL, 0)
INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (21, N'G', N'27/05/2023', NULL, 0)
INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (22, N'FG', N'27/05/2023', NULL, 0)
INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (23, N'ZZ', N'27/05/2023', NULL, 0)
INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (24, N'AA', N'27/05/2023', NULL, 0)
INSERT [dbo].[Category] ([CategoryID], [Name], [DateCreated], [DateModified], [Status]) VALUES (25, N'Ngoc', N'27/05/2023', NULL, 0)
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([ProductID], [CategoryID], [Name], [QuantityAvailable], [UnitPrice], [RamCapacity], [StorageCapacity], [CpuBrand], [VgaBrand]) VALUES (1, 3, N'ASUS TUF GAMING', 20, CAST(166.99 AS Decimal(10, 2)), N'8GB', N'526GB', N'AMD', NULL)
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
ALTER TABLE [dbo].[Feedback]  WITH CHECK ADD  CONSTRAINT [FK__Feedback__OrderI__48CFD27E] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Orders] ([OrderID])
GO
ALTER TABLE [dbo].[Feedback] CHECK CONSTRAINT [FK__Feedback__OrderI__48CFD27E]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK__OrderDeta__Order__3C69FB99] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Orders] ([OrderID])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK__OrderDeta__Order__3C69FB99]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK__OrderDeta__Produ__3B75D760] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK__OrderDeta__Produ__3B75D760]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK__Orders__AccountI__32E0915F] FOREIGN KEY([AccountID])
REFERENCES [dbo].[Account] ([AccountID])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK__Orders__AccountI__32E0915F]
GO
ALTER TABLE [dbo].[Post]  WITH CHECK ADD  CONSTRAINT [FK__Post__AccountID__2D27B809] FOREIGN KEY([AccountID])
REFERENCES [dbo].[Account] ([AccountID])
GO
ALTER TABLE [dbo].[Post] CHECK CONSTRAINT [FK__Post__AccountID__2D27B809]
GO
ALTER TABLE [dbo].[PostImg]  WITH CHECK ADD  CONSTRAINT [FK_PostImg_Post] FOREIGN KEY([PostID])
REFERENCES [dbo].[Post] ([PostID])
GO
ALTER TABLE [dbo].[PostImg] CHECK CONSTRAINT [FK_PostImg_Post]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK__Product__Categor__38996AB5] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([CategoryID])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK__Product__Categor__38996AB5]
GO
ALTER TABLE [dbo].[ProductIMG]  WITH CHECK ADD  CONSTRAINT [FK_ProductIMG_Product] FOREIGN KEY([ProID])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[ProductIMG] CHECK CONSTRAINT [FK_ProductIMG_Product]
GO
ALTER TABLE [dbo].[Voucher]  WITH CHECK ADD  CONSTRAINT [FK__Voucher__TypeID__403A8C7D] FOREIGN KEY([TypeID])
REFERENCES [dbo].[VoucherType] ([TypeID])
GO
ALTER TABLE [dbo].[Voucher] CHECK CONSTRAINT [FK__Voucher__TypeID__403A8C7D]
GO
ALTER TABLE [dbo].[VoucherProductLink]  WITH CHECK ADD  CONSTRAINT [FK__VoucherPr__Produ__45F365D3] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[VoucherProductLink] CHECK CONSTRAINT [FK__VoucherPr__Produ__45F365D3]
GO
ALTER TABLE [dbo].[VoucherProductLink]  WITH CHECK ADD  CONSTRAINT [FK__VoucherPr__Vouch__44FF419A] FOREIGN KEY([VoucherID])
REFERENCES [dbo].[Voucher] ([VoucherID])
GO
ALTER TABLE [dbo].[VoucherProductLink] CHECK CONSTRAINT [FK__VoucherPr__Vouch__44FF419A]
GO
ALTER TABLE [dbo].[VoucherUsage]  WITH CHECK ADD  CONSTRAINT [FK__VoucherUs__Order__4316F928] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Orders] ([OrderID])
GO
ALTER TABLE [dbo].[VoucherUsage] CHECK CONSTRAINT [FK__VoucherUs__Order__4316F928]
GO
ALTER TABLE [dbo].[VoucherUsage]  WITH CHECK ADD  CONSTRAINT [FK__VoucherUs__Vouch__4222D4EF] FOREIGN KEY([VoucherID])
REFERENCES [dbo].[Voucher] ([VoucherID])
GO
ALTER TABLE [dbo].[VoucherUsage] CHECK CONSTRAINT [FK__VoucherUs__Vouch__4222D4EF]
GO
